def main():
    print("Выберите нужные алгоритмы:\n",
          "1 - Быстрая сортировка\n",
          "2 - Сортировка Шелла\n",
          "3 - Сортировка выбором\n",
          "4 - Timsort\n")
    choice = (input("Введите номера алгоритмов через пробел >> \n")).split()
    try:
        for i in choice:
            if i == '1':
                from src.lab_one.benchmarks import benchmark_time_quick_sort
            elif i == '2':
                from src.lab_one.benchmarks import benchmark_time_shell_sort
            elif i == '3':
                from src.lab_one.benchmarks import benchmark_time_selection_sort
            else:
                from src.lab_one.benchmarks import benchmark_time_timsort

    except KeyError:
        raise ValueError('Некорректный ввод')
    print("Результаты работы программы можно наблюдать на сохраненных графиках выбранных сортировок.")


if __name__ == "__main__":
    main()
