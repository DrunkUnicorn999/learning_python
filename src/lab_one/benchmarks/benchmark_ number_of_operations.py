import pprofile
from src.lab_one.lib.my_sorted import quicksort
from random import randint
import sys
sys.setrecursionlimit(5000)

arr = [randint(-63, 64) for i in range(100)]


def someHotSpotCallable():
    # Deterministic profiler
    prof = pprofile.Profile()
    with prof():
        quicksort(arr)
    prof.print_stats()


print(someHotSpotCallable())
