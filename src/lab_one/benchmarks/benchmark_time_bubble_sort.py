import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from src.lab_one.lib.my_sorted import bubble_sort
from src.lab_one.lib.functions import elapsed_time


xdata = []
ydata = []

step = 100
min_size = 100
max_size = 1000

for n in range(min_size, max_size + step, step):
    xdata.append(n)
    x = sorted([e for e in range(n)], reverse=True)
    ydata.append(elapsed_time(bubble_sort, x))

print(xdata)
print(ydata)

xdata = np.array(xdata)
ydata = np.array(ydata)


def func(n, a, b, c):
    return a * (b * n) ** 2 + c


popt, pcov = curve_fit(func, xdata, ydata)
print(popt)

plt.style.use('seaborn')
fig, ax = plt.subplots()

ax.set_title('Сортировка пузырьком', fontsize=24, c='peachpuff')
ax.set_xlabel('Размер массива', fontsize=14, c='c')
ax.set_ylabel('Время выполнения', fontsize=14, c='peachpuff')

plt.plot(xdata, ydata, 'b-', label='data')

plt.plot(xdata, func(xdata, *popt), 'r-', label='data')

plt.plot(np.linspace(1_000, 5_000, 1_000), func(np.linspace(1_000, 5_000, 1_000), *popt), 'g-',
         label='data')

plt.show()
