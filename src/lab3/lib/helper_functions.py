def compare(first, second, choice=0):
    if choice == 0:
        return first == second
    elif choice == 1:
        return first > second
    elif choice == 2:
        return first < second
    elif choice == 3:
        return first != second
    if choice == 4:
        return first in second
    elif choice == 5:
        return first not in second
