import networkx as nx
from src.lab3.lib import graph
from src.lab3.const_values import GRAPH_10v_20u


test_graph_input = graph.Graph(GRAPH_10v_20u).shortest_path('I4', 'O8')
excepted_graph_input = nx.shortest_path(nx.Graph(GRAPH_10v_20u), 'I4', 'O8')


def test_graph_check_shortest_path():
    assert len(test_graph_input) == len(excepted_graph_input)
