from math import log, factorial

import numpy as np
from numpy import array


def constant_complexity(size, number=1):
    """Function for constant theoretical complexity"""
    return [number for e in range(size)]


def linear_complexity(array):
    """Function for linear theoretical complexity"""
    return array


def quadratic_complexity(array):
    """Function for quadratic theoretical complexity"""
    return [e**2 for e in array]


def cubic_complexity(array):
    """Function for cubic theoretical complexity"""
    return [e**3 for e in array]


def logarithmic_complexity(array):
    """Function for logarithmic theoretical complexity"""
    return [log(e, 2) for e in array]


def n_logarithm_n_complexity(array):
    """Function for custom logarithmic theoretical complexity"""
    return [e*log(e, 2) for e in array]


def n_logarithmic_power_complexity(array, deg=1):
    """Function for logarithmic theoretical complexity with degree for logarithm"""
    return [e*(log(e, 2))**deg for e in array]


def radix_complexity(array, radix=2):
    """Function for exponential theoretical complexity with custom radix"""
    return [radix**e for e in array]


def factorial_complexity(array):
    """Function for factorial theoretical complexity"""
    return [factorial(e) for e in array]


def linear_multiply_m_complexity(array, m):
    """Function for linear*m theoretical complexity"""
    return [i*m for i in array]


def v_e_complexity(array, edges):
    """Function for BFS complexity in Graph"""
    return [array[i]+edges[i] for i in range(len(array))]
