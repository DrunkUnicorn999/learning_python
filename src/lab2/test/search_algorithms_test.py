import pytest
from src.lab2.lib.search_algorithms import (LinearSearch, BinarySearch,
                                            NaiveStringSearch, KMPStringSearch)


# константы для тестов
array = [12, 3, 6, 5, 3, 83, 5, 2, 34, 14]
string = "Scientia potentia est"

# параметры для параметризированных тестов
params = [(LinearSearch(array, 5).search(), array.index(5)),
          (BinarySearch(array, 83).search(), array.index(83)),
          (NaiveStringSearch(string, 'ia').search(), string.find('ia')),
          (KMPStringSearch(string, 'po').search(), string.find('po'))]


@pytest.mark.parametrize("test_input, excepted", params)
def test_compare(test_input, excepted):
    assert test_input == excepted
