from setuptools import setup

with open('README', 'r') as readme_file:
    readme = readme_file.read()

setup(
    name='lab_one',
    version='1.0',
    description='first lab session',
    author='drunkunicorn999',
    author_email='drunkunicorn999@gmail.com',
    packages=['lab_one'],  # тоже, что и name
)
